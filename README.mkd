subvert
====================
replace words by FROM pattern and TO pattern.

its based on [tpope/vim-abolish](http://github.com/tpope/vim-abolish)

Usage
====================
	$ subvert [OPTION] FROM TO [FILE]...

	Options:
		--help       show this help message
		--version    print the version

Example
====================
	$ cat enquete.txt
	0001: dogs
	0002: cats
	0003: cats
	0004: cats
	0005: dogs
	0006: dogs

	$ cat enquete.txt | subvert {dog,cat}s {cat,dog}s
	0001: cats
	0002: dogs
	0003: dogs
	0004: dogs
	0005: cats
	0006: cats

	$ cat tour.txt
	rooteA: a->b->c
	rooteB: b->c->a
	rooteC: c->a->b

	$ subvert {a,b,c}->{b,c,a}->{c,a,b} {a,a,a}->{b,b,b}->{c,c,c} tour.txt
	rooteA: a->b->c
	rooteB: a->b->c
	rooteC: a->b->c

License
====================
MIT License

Author
====================
Kusabashira <kusabashira227@gmail.com>
