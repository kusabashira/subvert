package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
)

var (
	COMMAND_NAME      = "subvert"
	VERSION           = "0.1.0"
	NO_NECESSARY_ARGS = 2
	PATTERN_SEPARATOR = regexp.MustCompile("{|}")
	GROUP_SEPARATOR   = regexp.MustCompile(",")
)

func printVersion() {
	fmt.Fprintf(os.Stderr, "%s: %s\n", COMMAND_NAME, VERSION)
}

func printUsage() {
	fmt.Fprintf(os.Stderr, `
Usage: %s [OPTION] FROM TO [FILE]...
replace words by FROM pattern and TO pattern

Options:
	--help       show this help message
	--version    print the version

Report bugs to <kusabashira227@gmail.com>
`[1:], COMMAND_NAME)
}

func trimHeadAndTailSpaceString(s []string) []string {
	if 1 < len(s) && s[0] == "" {
		s = s[1:]
	}
	if 1 < len(s) && s[len(s)-1] == "" {
		s = s[:len(s)-1]
	}
	return s
}

func splitPattern(pattern string) [][]string {
	splittedPattern := PATTERN_SEPARATOR.Split(pattern, -1)
	splittedPattern = trimHeadAndTailSpaceString(splittedPattern)

	var groups [][]string
	for _, group := range splittedPattern {
		groups = append(groups, GROUP_SEPARATOR.Split(group, -1))
	}
	return groups
}

func checkIsRegularPatterns(from, to [][]string) error {
	if len(from) != len(to) {
		return fmt.Errorf("mismatch number of pattern")
	}
	for i, _ := range from {
		if len(from[i]) != len(to[i]) {
			return fmt.Errorf("mismatch number of selection")
		}
	}
	return nil
}

func newReplaceMap(from, to [][]string) (replaceMap []map[string]string) {
	for gr_i, group := range from {
		replaceMap = append(replaceMap, make(map[string]string))
		for se_i, _ := range group {
			replaceMap[gr_i][from[gr_i][se_i]] =
				to[gr_i][se_i]
		}
	}
	return replaceMap
}

func newMatcher(from, to [][]string) (*regexp.Regexp, error) {
	var rawMatcherList []string
	for _, group := range from {
		var rawMatcher []string
		for _, selection := range group {
			rawMatcher = append(rawMatcher,
				regexp.QuoteMeta(selection))
		}
		rawMatcherList = append(rawMatcherList,
			strings.Join(rawMatcher, "|"))
	}
	for i, rawMatcher := range rawMatcherList {
		rawMatcherList[i] = "(" + rawMatcher + ")"
	}
	return regexp.Compile(strings.Join(rawMatcherList, ""))
}

type Replacer struct {
	matcher    *regexp.Regexp
	replaceMap []map[string]string
}

func NewReplacer(fromPattern, toPattern string) (*Replacer, error) {
	from := splitPattern(fromPattern)
	to := splitPattern(toPattern)
	err := checkIsRegularPatterns(from, to)
	if err != nil {
		return nil, err
	}

	replaceMap := newReplaceMap(from, to)
	matcher, err := newMatcher(from, to)
	if err != nil {
		return nil, err
	}
	return &Replacer{
		replaceMap: replaceMap,
		matcher:    matcher,
	}, nil
}

func (r *Replacer) Replace(s string) string {
	return r.matcher.ReplaceAllStringFunc(s, func(in string) string {
		var ls []string
		m := r.matcher.FindStringSubmatch(in)[1:]
		for i, from := range m {
			ls = append(ls, r.replaceMap[i][from])
		}
		return strings.Join(ls, "")
	})
}

func _main() error {
	var (
		isHelp    = flag.Bool("help", false, "show this help message")
		isVersion = flag.Bool("version", false, "print the version")
	)
	flag.Usage = printUsage
	flag.Parse()

	switch {
	case *isHelp:
		printUsage()
		return nil
	case *isVersion:
		printVersion()
		return nil
	}

	var fromPattern, toPattern string
	switch flag.NArg() {
	case 0:
		return fmt.Errorf(`no specify "FROM pattern" and "TO pattern"`)
	case 1:
		return fmt.Errorf(`no specify "TO pattern"`)
	}
	fromPattern, toPattern = flag.Arg(0), flag.Arg(1)

	var input io.Reader
	if flag.NArg() <= NO_NECESSARY_ARGS {
		input = os.Stdin
	} else {
		var inputs []io.Reader
		for _, fname := range flag.Args()[NO_NECESSARY_ARGS:] {
			inFile, err := os.Open(fname)
			if err != nil {
				return err
			}
			defer inFile.Close()
			inputs = append(inputs, inFile)
		}
		input = io.MultiReader(inputs...)
	}

	replacer, err := NewReplacer(fromPattern, toPattern)
	if err != nil {
		return err
	}
	reader := bufio.NewScanner(input)
	for reader.Scan() {
		line := replacer.Replace(reader.Text())
		fmt.Println(line)
	}

	return nil
}

func main() {
	err := _main()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %v", COMMAND_NAME, err)
		os.Exit(1)
	}
}
